db.room.find()

db.room.insertOne ({
	name: 'single',
	accomodate: 2,
	price: 1000,
	description: 'A simple room with all the basic neccessities',
	room_available: 10,
	isAvailabale: false
})

db.room.insertMany([
	{
	name: 'double',
	accomodate: 3,
	price: 2000,
	description: 'A room for for a small family going on a vacation',
	room_available: 5,
	isAvailabale: false
},
{
	name: 'queen',
	accomodate: 4,
	price: 4000,
	description: 'A room with a queen sized bed perfect for a simple getaway',
	room_available: 15,
	isAvailabale: false
}

])

// no. 5
db.room.find({name: 'double'}) 
// no. 6
db.room.updateOne(
	{name: 'double'},
	{
		$set: {room: 0}
	}

	)
// no. 7
db.room.deleteMany({room: 0})


